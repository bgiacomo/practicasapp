package com.acme.practicasapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;

import model.Player;
import model.Question;


public class ActivityPlay extends ActionBarActivity {

    private ArrayList <Question> list =new ArrayList<Question>();
    private TextView question;
    private Button firstButton;
    private Button secondButton;
    private Button thirdButton;
    private Button fourthButton;
    private Question actualQuestion;
    private TextView price;
    private TextView numberOfQuestion;
    private ArrayList<Button> buttonList=new ArrayList<Button>();
    private Player player;
    private SharedPreferences saveData;

    int counting=0;
    int countingPrice=0;
    int[] arrayPrice = new int[]{0, 100, 200, 300, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000, 125000, 250000, 500000, 1000000};
    int help=0; //everytime the user ask for help this variable is incremented
    int prefHelp=0;

    //Every boolean is activated when the user ask for help

    boolean telBool=false;
    boolean fiftyBool=false;
    boolean homeBool=false;

    private static final String FILE_QUESTIONS = "questions.xml";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_play);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //I need to know how many times the user can use helps

        SharedPreferences pref=getSharedPreferences("file_preferences", Context.MODE_PRIVATE);
        prefHelp=pref.getInt("Helper",1)+1; //Preferences save the item position, so I have to add 1 unit.

        //references to the 4 buttons that contains the answers

        this.question=(TextView)findViewById(R.id.pregunta);
        this.firstButton=(Button)findViewById(R.id.answer1);
        this.secondButton=(Button)findViewById(R.id.answer2);
        this.thirdButton=(Button)findViewById(R.id.answer3);
        this.fourthButton=(Button)findViewById(R.id.answer4);
        this.price=(TextView)findViewById(R.id.playFor);
        this.numberOfQuestion=(TextView)findViewById(R.id.questionNum);



        addTagsToButtons();
        createQuestions();


        updateScreen();
        //assign the current question taken from the array of questions
        actualQuestion=list.get(counting);


        //listeners to the button
        this.firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TAG:",firstButton.getTag().toString());
                Log.d("RIGHT",actualQuestion.getRight().toString());
                if(firstButton.getTag().toString().equals(actualQuestion.getRight())){
                    countingPrice++;
                    price.setText(" " + arrayPrice[countingPrice] + "$");

                }else{
                    wrongAnswer();

                }
                updateScreen();
            }
        });

        this.secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Log.d("TAG:",secondButton.getTag().toString());
                Log.d("RIGHT",actualQuestion.getRight());

                if(secondButton.getTag().toString().equals(actualQuestion.getRight())){
                    countingPrice++;
                    price.setText(" " + arrayPrice[countingPrice] + "$");

                }else{
                    wrongAnswer();

                }
                updateScreen();
            }
        });
        this.thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TAG:",thirdButton.getTag().toString());
                Log.d("RIGHT",actualQuestion.getRight());
                if(thirdButton.getTag().toString().equals(actualQuestion.getRight())){
                    countingPrice++;
                    price.setText(" " + arrayPrice[countingPrice] + "$");

                }else{
                   wrongAnswer();

                }
                updateScreen();
            }
        });
        this.fourthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("TAG:",fourthButton.getTag().toString());
                Log.d("RIGHT",actualQuestion.getRight());
                if(fourthButton.getTag().toString().equals(actualQuestion.getRight())){
                    countingPrice++;
                    price.setText(" " + arrayPrice[countingPrice] + "$");
                    /*Toast.makeText(getApplicationContext(), "giusta", Toast.LENGTH_SHORT)
                            .show();*/

                }else{
                   wrongAnswer();
                    /*Toast.makeText(getApplicationContext(), "sbagliata", Toast.LENGTH_SHORT)
                            .show();*/
                }

                updateScreen();
            }
        });

        //Using an array of Button I can iterate them.
        buttonList.add(this.firstButton);
        buttonList.add(this.secondButton);
        buttonList.add(this.thirdButton);
        buttonList.add(this.fourthButton);

    }

    //this methos is necessary to understand how many money the user losts for a bad answer
    public void wrongAnswer(){

        if(countingPrice<5){
            countingPrice=0;
        }
        else if(countingPrice>=5 && countingPrice<10) {
            countingPrice = 5;
        }else if(countingPrice>=10){
            countingPrice=10;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int tabAction=item.getItemId();

        switch (tabAction) {
            // action with ID action_refresh was selected
            case R.id.action_tel:
                if(!telBool && help<prefHelp){
                Log.d("QUIZZAPP","telephone help");
                telephoneHelp();
                telBool=!telBool;}
                else{
                    Toast.makeText(getApplicationContext(), R.string.help, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            // action with ID action_settings was selected
            case R.id.action_fifty:
                if(!fiftyBool && help<prefHelp) {
                    Log.d("QUIZZAPP", "50% help");
                    fiftyHelp();
                    fiftyBool=!fiftyBool;
                }else{
                    Toast.makeText(getApplicationContext(), R.string.help, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case R.id.action_help:
                if(!homeBool && help<prefHelp) {
                    Log.d("QUIZZAPP", "home help");
                    homeHelp();
                    homeBool=!homeBool;
                }else {
                    Toast.makeText(getApplicationContext(), R.string.help, Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            case R.id.action_exit:
                Log.d("QUIZZAPP","exit");
                /*
                Here I have to save all the scores!!
                */
                exitGame();
                break;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }



        return true;
    }

    private void addTagsToButtons(){

        firstButton.setTag(1);
        secondButton.setTag(2);
        thirdButton.setTag(3);
        fourthButton.setTag(4);

    }

    private void updateScreen(){

        enableAllButtons();

    Log.d("COUNTING-PRICE",""+countingPrice);

        //control for the final question
        if(counting==list.size()){
            firstButton.setEnabled(false);
            secondButton.setEnabled(false);
            thirdButton.setEnabled(false);
            fourthButton.setEnabled(false);
            exitGame();
        }else {

            //it's not the last question: go on!

            //assign the current question taken from the array of questions
            actualQuestion = list.get(counting);

            question.setText(actualQuestion.getText());
            firstButton.setText(actualQuestion.getAns1());
            secondButton.setText(actualQuestion.getAns2());
            thirdButton.setText(actualQuestion.getAns3());
            fourthButton.setText(actualQuestion.getAns4());

            numberOfQuestion.setText(" " + (counting));

            if (countingPrice == 0) {
                price.setText(" " + arrayPrice[0] + "$");
            }else{
                price.setText(" "+arrayPrice[countingPrice] + "$");
            }


            counting++;
        }

    }

    private void createQuestions() {

        try{

            XmlResourceParser xrp = getResources().getXml(R.xml.questions);

            xrp.next();
            int eventType = xrp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType){

                    case XmlPullParser.START_DOCUMENT:

                        break;

                    case XmlPullParser.START_TAG:
                            if(xrp.getName().equalsIgnoreCase("question")){

                                Question q=new Question(xrp.getAttributeValue(0),xrp.getAttributeValue(1),xrp.getAttributeValue(2),xrp.getAttributeValue(3),xrp.getAttributeValue(4),xrp.getAttributeValue(5),xrp.getAttributeValue(6),xrp.getAttributeValue(7),xrp.getAttributeValue(8),xrp.getAttributeValue(9),xrp.getAttributeValue(10));
                                list.add(q);
                            }
                        break;

                    case XmlPullParser.END_DOCUMENT:

                        break;

                    default:
                        break;
                }


                eventType = xrp.next();
            }

        }catch (Exception e){
            Log.d("Exception","FILE NOT FOUND");
        }

    }

    private void telephoneHelp(){

        help++;

        String tel=actualQuestion.getPhone();

        for (Button but: buttonList){

            if(!tel.equals(but.getTag().toString())){
                but.setEnabled(false);
            }
        }


    }

    private void fiftyHelp(){

        help++;

        String f1=actualQuestion.getFifty1();
        String f2=actualQuestion.getFifty2();


        //step 1: looking for fifty1 and disable it
        for (Button but:buttonList){
            if(f1.equals(but.getTag().toString())){
                but.setEnabled(false);
            }
        }
        //step 2: looking for fifty2 and disable it
        for (Button but:buttonList){
            if(f2.equals(but.getTag().toString())){
                but.setEnabled(false);
            }
        }


    }

    private void homeHelp(){

        help++;

        String aud=actualQuestion.getAudience();

        for (Button but: buttonList){

            if(!aud.equals(but.getTag().toString())){
                but.setEnabled(false);
            }
        }

    }

    private void exitGame(){

        SharedPreferences preferences = getSharedPreferences("file_preferences", Context.MODE_PRIVATE);
        String user=preferences.getString("Alias",null);

        if(user==null){
            user="default_user";
        }

        player=new Player(user,arrayPrice[countingPrice]);
        /*Toast.makeText(getApplicationContext(), player.getName()+" "+player.getScore(), Toast.LENGTH_SHORT)
                .show();*/

        SQLiteDatabase db=this.openOrCreateDatabase("localDB",MODE_PRIVATE,null);

        try{
            db.beginTransaction();
            db.execSQL("CREATE TABLE IF NOT EXISTS Scores(Username VARCHAR,Score INTEGER);");
            db.execSQL("INSERT INTO Scores VALUES('"+player.getName()+"','"+
                    player.getScore()+"'"+");");
            db.setTransactionSuccessful();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT)
                .show();

        }finally {
            db.endTransaction();
        }

        //send my score to the webserver
        new sendMyScore().execute(player.getName(),Integer.toString(player.getScore()));

        //torno indietro
        saveData=null;
        onBackPressed();


    }

    private void enableAllButtons(){

        for(Button but:buttonList){
            but.setEnabled(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.d("Quizzapp: ","I am saving data..");
        saveData=getSharedPreferences("saveData",MODE_PRIVATE);
        SharedPreferences.Editor editor=saveData.edit();

        editor.putInt("counting",counting);
        editor.putInt("countingPrice",countingPrice);
        editor.putBoolean("tel",telBool);
        editor.putBoolean("fifty",fiftyBool);
        editor.putBoolean("home",homeBool);
        editor.commit();


    }


}

