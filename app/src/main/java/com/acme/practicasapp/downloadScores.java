package com.acme.practicasapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Giacomo on 10/03/15.
 */
public class downloadScores extends AsyncTask<String, String, Void> {

    private final String PREFERENCES_FILE = "file_preferences";
    private final String PREFERENCES_ALIAS = "Alias";
    private final String PREFERENCES_HELP = "Helper";
    private final String PREFERENCES_FRIENDNAME = "FriendName";

    private Context mContext;

    private String jsonString="";
    private StringBuffer sb;

    @Override
    public Void doInBackground(String... params) {


        Log.d("Quizzapp: ","Listener Attivo");

        String username = params[0];

        List<NameValuePair>  pairs = new ArrayList<NameValuePair>();

        pairs.add(new BasicNameValuePair("name",username));
        String response=null;
        URL url = null;
        HttpURLConnection con=null;
        try {
            url = new URL("http://wwtbamandroid.appspot.com/rest/highscores"
                     +  "?"  +  URLEncodedUtils.format(pairs, "utf-8"));
            Log.d("QuizzappURL",url.toString());
            con  =  (HttpURLConnection)  url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept",  "application/json");
            BufferedReader reader  =  new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            sb  =  new StringBuffer();
            String s  =  null;
            while  ((s  =  reader.readLine())   !=  null)  {

                sb.append(s);

            }
            reader.close();
            response  =  sb.toString();
            Log.d("QuizzAppJSONtoPass",""+response);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            con.disconnect();
        }



        jsonString=response;
        Log.d("QuizzAppJSONtoString",""+jsonString);
        return null;

    }

    public StringBuffer getSb() {
        return sb;
    }

    public String getJsonString() {
        return jsonString;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void s) {
        Log.d("ciaobel","ciaobel");
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
}
