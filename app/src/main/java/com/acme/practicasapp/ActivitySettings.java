package com.acme.practicasapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class ActivitySettings extends ActionBarActivity {

    private final String PREFERENCES_FILE = "file_preferences";
    private final String PREFERENCES_ALIAS = "Alias";
    private final String PREFERENCES_HELP = "Helper";
    private final String PREFERENCES_FRIENDNAME = "FriendName";
    SharedPreferences preferences;

    private Spinner help;
    private Button addFriend;
    private EditText etFriend;
    private EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_settings);

        this.help=(Spinner)findViewById(R.id.etNumber);
        Integer[] items = new Integer[]{1,2,3};
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item, items);
        help.setAdapter(adapter);

        getSupportActionBar().setTitle("Settings");

        this.addFriend=(Button)findViewById(R.id.addFriend);
        this.etFriend=(EditText)findViewById(R.id.etFriend);
        this.etName=(EditText)findViewById(R.id.etSettings);


        this.addFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!checkFriendName()){
                    Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT)
                .show();
                }else{
                    Log.d("QuizzApp","sending data to the server..");
                    sendDataToServer(etName.getText().toString(),etFriend.getText().toString());
                }


            }
        });

    }

    private boolean checkFriendName(){

        this.etFriend=(EditText)findViewById(R.id.etFriend);

        if(this.etFriend.getText().toString().isEmpty())
            return false;
        else
            return true;

    }


    private void sendDataToServer(String alias,String friendName){

        //You have to test internet connection on a real device, not the emulator!

        if(isConnected()){
            Log.d("Quizzapp","Connected");

            new  addFriend().execute(alias,friendName);

        }else {
            Toast.makeText(getApplicationContext(), "Check your internet connection", Toast.LENGTH_SHORT)
                    .show();
        }

    }

    private boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        SharedPreferences.Editor prefEditor =preferences.edit();
        prefEditor.putString(PREFERENCES_ALIAS,((EditText)findViewById(R.id.etSettings)).getText().toString());

        prefEditor.putString(PREFERENCES_FRIENDNAME,((EditText)findViewById(R.id.etFriend)).getText().toString());

        prefEditor.putInt(PREFERENCES_HELP, help.getSelectedItemPosition());

        prefEditor.commit();


    }

    @Override
    protected void onResume() {
        super.onResume();

        //prova commento
        preferences = getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);

        EditText alias=(EditText)findViewById(R.id.etSettings);
        alias.setText(preferences.getString(PREFERENCES_ALIAS,null));


        EditText friend=(EditText)findViewById(R.id.etFriend);
        friend.setText(preferences.getString(PREFERENCES_FRIENDNAME,null));

        int pos=preferences.getInt(PREFERENCES_HELP,-1);
        help.setSelection(pos);



    }
}
