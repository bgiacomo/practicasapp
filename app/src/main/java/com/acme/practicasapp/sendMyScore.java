package com.acme.practicasapp;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import model.HighScore;


/**
 * Created by Giacomo on 10/03/15.
 */
public class sendMyScore extends AsyncTask<String,String,String> {

    @Override
    protected String doInBackground(String... params) {


        String alias = params[0];
        String score = params[1];
        HighScore hs=new HighScore(alias,score,null,null);
        Log.d("Quizzapp", "received: " + alias + " " + score);

        List<NameValuePair> pairs = new ArrayList<>();

        pairs.add(new BasicNameValuePair("name", alias));
        pairs.add(new BasicNameValuePair("score", score));

        Log.d("Quizzapp", "received: " + pairs.get(0).toString() + " " + pairs.get(1).toString());

        String link = "http://wwtbamandroid.appspot.com/rest/highscores";
        Log.d("QuizzappLink",link);


        HttpURLConnection con = null;
        try{

            URL url = new URL(link);
            Log.d("quizzappURL: ",url.toString());
            con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("PUT");
            con.setDoOutput(true);
            con.connect();

            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(con.getOutputStream(), "UTF-8"));
            writer.write(URLEncodedUtils.format(pairs, "UTF-8"));

            writer.close();
            con.getInputStream();

            Log.d("quizapperr: ",con.getErrorStream().toString());
            Log.d("quizzapp: ","code: "+con.getResponseCode());


        }catch (Exception e){
            e.toString();
        }finally {
            con.disconnect();
        }

        Log.d("quizzapp: ","todo corecto");
        return "todo corecto";
    }

        @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);



    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }
}
