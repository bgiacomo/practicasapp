package com.acme.practicasapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import model.HighScore;
import model.HighScoreList;


public class ActivityScores extends ActionBarActivity {

    private ArrayAdapter<String> listAdapter ;
    private ListView friendList;
    private ListView myList;
    private downloadScores ds;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_scores);
        getSupportActionBar().setTitle("Scores");

        myList=(ListView)findViewById(R.id.myList);
        friendList=(ListView)findViewById(R.id.friendList);

        TabHost host = (TabHost) findViewById(R.id.tabHost);
        host.setup();

        TabHost.TabSpec spec = host.newTabSpec("TAB1");
        spec.setIndicator("Local", getResources().getDrawable(R.drawable.ic_local));
        spec.setContent(R.id.tab1);
        host.addTab(spec);

        spec = host.newTabSpec("TAB2");
        spec.setIndicator("Global", getResources().getDrawable(R.drawable.ic_global));
        spec.setContent(R.id.tab2);
        host.addTab(spec);


        Log.d("ciao","ciao");

        db=this.openOrCreateDatabase("localDB",MODE_PRIVATE,null);

        ArrayList<String> arrayHitList=new ArrayList<>();

        try{

            Cursor c1=db.rawQuery("SELECT * FROM Scores ORDER BY Score DESC LIMIT 10",null);
            String s="";
            while (c1.moveToNext()){

                s = c1.getString(c1.getColumnIndex("Username"))+" - "+c1.getString(c1.getColumnIndex("Score"));
                Log.d("DB:", s);

                arrayHitList.add(s);

            }
            c1.close();
            db.close();


        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "No data to show", Toast.LENGTH_SHORT)
                    .show();

        }finally {
        }



        // Create ArrayAdapter using the list.
        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, arrayHitList);

        myList.setAdapter( listAdapter );
        final String jsonString;
        host.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                    Log.d("QuizzApp tab: ",tabId);

                    if(tabId.equals("TAB2")){



                        SharedPreferences pref=getSharedPreferences("file_preferences",MODE_PRIVATE);
                        String un=pref.getString("Alias",null);
                        if(un==""){
                            Toast.makeText(getApplicationContext(), "Please insert a valid name", Toast.LENGTH_SHORT)
                                    .show();
                        }
                        Log.d("pregGestrsg",""+un);
                        ArrayList<String> arr=new ArrayList<>();
                        ds=new downloadScores();
                        ds.execute(un);
                        //devo attendere che il thread finisca..

                        while (ds.getJsonString().isEmpty()) {
                            //do nothing..
                            Log.d("sono vuoto","sono vuoto");
                        }
                        Log.d("QuizzappJSON: ", "" + ds.getJsonString().toString());
                        createTableFriends(ds.getJsonString().toString());

                    }else{


                    }



            }
        });




    }

    public void createTableFriends(String s){



            //fare controllo che non sia null
            Log.d("quizzAppString:", ""+s);
            try {

                JSONObject obj = new JSONObject(s);

                JSONArray jsonArray = obj.getJSONArray("scores");

                HighScoreList hsList=new HighScoreList();


                ArrayList<HighScore> myList = new ArrayList<HighScore>();

                ArrayList<String> stringArray=new ArrayList<>();

                for (int i=0;i<jsonArray.length();i++){
                    obj=jsonArray.getJSONObject(i);
                    HighScore score=new HighScore(obj.getString("name"),obj.getString("scoring"),null,null);
                    stringArray.add(obj.getString("name")+"  " +obj.getString("scoring"));
                    myList.add(score);
                }

                Collections.sort(myList, new Comparator<HighScore>() {
                    @Override
                    public int compare(HighScore lhs, HighScore rhs) {
                        return rhs.getScoring().compareTo(lhs.getScoring());
                    }
                });


                stringArray.clear();
                for (int i=0;i<myList.size();i++){
                    stringArray.add(myList.get(i).getName()+" - "+ myList.get(i).getScoring());
                }

                hsList.setScores(myList);

                /*if (jsonArray != null) {
                    int len = jsonArray.length();
                    for (int i = 0; i < len; i++) {
                        myList.add(jsonArray.get(i).toString());
                    }
                }*/

                listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, stringArray);
                friendList.setAdapter(listAdapter);


            } catch (Exception e) {
                e.toString();
            }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_scores, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_trash) {

            new AlertDialog.Builder(this)
                    .setTitle("Attention!")
                    .setMessage("Are you sure you want to delete local scores?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deleteScores();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing..
                        }
                    })

                    .show();



            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteScores(){
        Log.d("quizzapp:","deleting db..");

        getApplicationContext().deleteDatabase("localDB");
        updateTab1();
    }

    private void updateTab1(){

        ArrayList<String> arrayHitList=new ArrayList<>();

        // Create ArrayAdapter using the list.
        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, arrayHitList);

        myList.setAdapter( listAdapter );

    }
}
