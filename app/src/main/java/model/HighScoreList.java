package model;

/**
 * Created by Giacomo on 12/03/15.
 */
import java.util.List;

public class HighScoreList {

    private List<HighScore> scores;

    public List<HighScore> getScores() {
        return scores;
    }

    public void setScores(List<HighScore> scores) {
        this.scores = scores;
    }

}
