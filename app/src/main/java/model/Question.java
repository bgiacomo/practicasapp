package model;

public class Question {

    private String ans1;
    private String ans2;
    private String ans3;
    private String ans4;
    private String audience;
    private String fifty1;
    private String fifty2;
    private String number;
    private String phone;
    private String right;
    private String text;

    public Question(){

    }

    public Question(String ans1, String ans2, String ans3, String ans4, String audience, String fifty1, String fifty2, String number, String phone, String right, String text) {
        this.ans1 = ans1;
        this.ans2 = ans2;
        this.ans3 = ans3;
        this.ans4 = ans4;
        this.audience = audience;
        this.fifty1 = fifty1;
        this.fifty2 = fifty2;
        this.number = number;
        this.phone = phone;
        this.right = right;
        this.text = text;
    }

    public String getAns1() {
        return ans1;
    }

    public void setAns1(String ans1) {
        this.ans1 = ans1;
    }

    public String getAns2() {
        return ans2;
    }

    public void setAns2(String ans2) {
        this.ans2 = ans2;
    }

    public String getAns3() {
        return ans3;
    }

    public void setAns3(String ans3) {
        this.ans3 = ans3;
    }

    public String getAns4() {
        return ans4;
    }

    public void setAns4(String ans4) {
        this.ans4 = ans4;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getFifty1() {
        return fifty1;
    }

    public void setFifty1(String fifty1) {
        this.fifty1 = fifty1;
    }

    public String getFifty2() {
        return fifty2;
    }

    public void setFifty2(String fifty2) {
        this.fifty2 = fifty2;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
